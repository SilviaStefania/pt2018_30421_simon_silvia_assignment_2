package PT2018.Model;


import PT2018.View.TaskView;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {

    private List<Server> servers  = new ArrayList<>();
    private int maxNrOfServers;
    private int maxNrTasksPerServer;
    private Strategy str = new Strategy();

    private List<Thread> threds  = new ArrayList<>();


    private TaskView theVi;

    public Scheduler(int maxNrOfServers, int maxNrTasksPerServer, TaskView theView){
        this.maxNrOfServers = maxNrOfServers;
        this.maxNrTasksPerServer = maxNrTasksPerServer;
        this.theVi= theView;
        for(int i= 0; i< this.maxNrOfServers; i++ ){
            Server s = new Server(theView, i, this.maxNrTasksPerServer);
            servers.add(s);

            Thread t= new Thread(s);
            t.start();

            threds.add(t);

        }
    }



    public void dispatchTask (Task t){

        str.addTask(getCozi(), t);
    }

    public List<Server> getCozi() {
        return servers;
    }


/*
  public int getPeakTime(int timeLimit){
        int maxTime= 0;
        for(int i  = 0; i<timeLimit; i++){
            int maxByCurrentTime = 0;
            Iterator<Server> its = servers.iterator();
            Server aux = null;
            while(its.hasNext()){
                aux= its.next();
                BlockingQueue<Task> auxTask = aux.getClienti();
                Iterator<Task> itT = auxTask.iterator();
                Task auxT;
                while(itT.hasNext()){
                    auxT=itT.next();
                    if(auxT.getArrivalTime()== i){
                        maxByCurrentTime+=1;
                    }
                }
            }
            if(maxByCurrentTime<maxTime){
                maxTime=maxByCurrentTime;
            }


        }
        return maxTime;
    }  */


    public float getAverageEmptyTime()
    {
        float avg = 0;
        for(Server server : servers)
        {
            avg += server.getEmptyTime();
        }
        return avg/servers.size();
    }

    public float getAverageServiceTime() {
        int sume = 0;
        for(Server server : servers)
        {
            sume += server.getAuxWait();
        }
        return (float)sume/servers.size();
    }



    public void stopTheTh(){
        for(Thread x: threds){
            x.interrupt();
        }
    }

}
