
package PT2018.Model;

import PT2018.View.TaskView;

import java.util.*;

import static java.lang.Thread.sleep;


public class SimulationManager implements Runnable {

    // private TaskView view = new TaskView();


    private int timeLimit ;
    private int maxProcessingTime;
    private int minProcessingTime  ;
    private int nrOfServers ; // cu cate incep deshise
    private int numberOfClients ;
    private int maxArrTime;
    private int minArrTime  ;
    private Strategy str;

    private float avServTime;
    private float avWaitTime;
    private int peakTime;
    private float emptyTime;


    private Scheduler scheduler;

    private TaskView taskView;

    private ArrayList<Task> generatedTasks = new ArrayList<>();



    public SimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int nrOfServers, int numberOfClients, int maxArrTime, int minArrTime, TaskView theView){

        this.timeLimit = timeLimit;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.nrOfServers = nrOfServers;
        this.numberOfClients = numberOfClients;
        this.minArrTime = minArrTime;
        this.maxArrTime = maxArrTime;
        this.taskView = theView;

        scheduler = new Scheduler(this.nrOfServers, this.numberOfClients, this.taskView);
        //for (int i = 0; i< this.nrOfServers; i++){
        //  Thread t1 = new Thread();
        // t1.start();

        str= new Strategy();
        //}

        System.out.println(this.timeLimit);   // pentru verificare, da, ajunge aici si ia ce trebuie

        // this.taskView = new TaskView();      //daca las asta, dupa ce apas pe start, apare iar fereastra



        generateNRandomTasks();




    }


    public float getEmptyTime()
    {
        return emptyTime;
    }

    private void generateNRandomTasks(){

        for(int i=0; i<this.numberOfClients; i++){

            Random setServiceTime=new Random();
            int serviceTime = setServiceTime.nextInt(maxProcessingTime - minProcessingTime +1)+ minProcessingTime;

            Random setArrTime = new Random();
            int arrivalTime =  setArrTime.nextInt( maxArrTime -minArrTime +1)+ minArrTime;


            Task t = new Task(arrivalTime, serviceTime, i);



            //   System.out.println("sa vedem "+ serviceTime);   //face si aici bine //////veridicare

            generatedTasks.add(t);



        }




        Collections.sort(generatedTasks, new SortByArrTime());
        int moment = 0;
        int max = -32000;
        for(int i = 0; i < this.timeLimit; i++)
        {
            int count = 0;
            for(Task t : generatedTasks)
            {
                if (t.getArrivalTime() == i)
                {
                    count++;
                }
            }
            if(count > max)
            {
                max = count;
                moment = i;
            }
        }
        peakTime = moment;


    }
    class SortByArrTime implements Comparator<Task> {

        public int compare(Task a, Task b) {
            return a.getArrivalTime() - b.getArrivalTime();

        }
    }

    public float getAverageWaitingTime()
    {
        float avg = 0;
        for(Task task : generatedTasks)
        {
            avg += task.getFinishTime();
        }
        System.out.println(avg);
        System.out.println(generatedTasks.size());
        return avg / generatedTasks.size();
    }





    @Override
    public void run() {
        int currentTime= 0;


        while (currentTime < this.timeLimit){
            Task aux = null;
            Iterator<Task> it = generatedTasks.iterator();
            while(it.hasNext()){
                aux=it.next();
                if(aux.getArrivalTime() == currentTime){
                    scheduler.dispatchTask(aux);
                    it.remove();

                }

            }


            currentTime +=1;
            System.out.println(" \n curent time " + currentTime);
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        scheduler.stopTheTh();



        avServTime= scheduler.getAverageServiceTime();

        this.emptyTime = scheduler.getAverageEmptyTime();

        System.out.println(avServTime + " "+ avWaitTime +" "+ peakTime);

        taskView.setPeakTime(Integer.toString(peakTime));
        taskView.setAverageWaitTime(Double.toString(getAverageWaitingTime()));
        taskView.setAverageServTime(Double.toString(avServTime));
        taskView.setEmptyTime(Double.toString(emptyTime));





    }


    public String getAvServTime() {
        StringBuilder s = new StringBuilder();
        s.append("");
        s.append(this.avServTime);
        return s.toString();
    }

    public String getAvWaitTime() {
        StringBuilder s = new StringBuilder();
        s.append("");
        s.append(this.avWaitTime);
        return s.toString();
    }

    public String getPeak() {
        StringBuilder s = new StringBuilder();
        s.append("");
        s.append(this.peakTime);
        return s.toString();
    }



}
