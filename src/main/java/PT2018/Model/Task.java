package PT2018.Model;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class Task {

    private int arrivalTime;
    private int serviceTime;
    private int finishTime;


    private int id = 0;


    public Task(int arrivalTime, int serviceTime, int id){

        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
        this.finishTime = arrivalTime + serviceTime;
        this.id = id;

    }



    public int getArrivalTime (){
        return arrivalTime;
    }

    public int getServiceTime(){
        return serviceTime;
    }

    //doar daca el e primul  in coada


    public void setFinishTime(int x){
        finishTime = x;

    }

    public int getFinishTime(){
        return finishTime;
    }



    public int modifyWaitTime(){
        return (finishTime - arrivalTime - serviceTime);
    }

    public String makeTaskToStrig(){
        return "T"+getID();
    }


    //e aceesi cu Task[] getTask de la profa ca vreau doar sa afisez id ul pe gui
    public int getID() {
        return id;
    }

}
