package PT2018.Model;



import PT2018.View.TaskView;

import java.util.Iterator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import static java.lang.Thread.sleep;

public class Server implements Runnable {

    private BlockingQueue<Task> clienti;
    private int waitingTime; //specific ptn o coada
    //WT = suma din FT-(arrival+ service) la toti cl
    //averageul e WT / nr clienti
    private int nrQ;
    private int auxWait;
    int maxNrTasksPerServer;
    private int emptyTime;
    private boolean emptyAux;

    public int getNrQ() {
        return nrQ;
    }

    private TaskView th;


    public Server(TaskView theView, int qnumber, int maxNrTasksPerServer){
        //init queue si waiting time
        this.auxWait = 0;
        this.emptyAux = true;
        this.maxNrTasksPerServer = maxNrTasksPerServer;
        this.clienti = new ArrayBlockingQueue<Task>(maxNrTasksPerServer);
        this.th = theView;
        this.waitingTime=0;  //la inceput e zero
        this.nrQ =qnumber;
    }

    public BlockingQueue<Task> getClienti() {
        return clienti;
    }

    public int getSize(){
        return clienti.size();
    }

    public void addTask(Task newTask){
        if(emptyAux == true)
        {
            this.emptyAux = false;
            this.emptyTime = newTask.getArrivalTime();
        }
        int inLine = 0;
        Iterator<Task> it= clienti.iterator();
        Task aux;
        if (clienti.size() == 0) {
            newTask.setFinishTime(newTask.getServiceTime() + newTask.getArrivalTime());
        } else {
            while (it.hasNext()) {
                aux = it.next();
                inLine++;
                newTask.setFinishTime(newTask.getServiceTime() + aux.getArrivalTime() + this.waitingTime);
            }
        }

        clienti.add(newTask);

        this.waitingTime+= newTask.modifyWaitTime();
        auxWait += newTask.getServiceTime();

        System.out.println("Task "+ newTask.getID() + " was added    arrival time: "+ newTask.getArrivalTime()  + "    service time: " + newTask.getServiceTime() +"        fin time: "+ newTask.getFinishTime() + "   Place in line" + inLine );

        String x= "Q"+nrQ+" ";
        Iterator<Task> tsk = clienti.iterator();
        while(tsk.hasNext()){
            Task t  = tsk.next();
            x += t.makeTaskToStrig();
            x+=" ";

        }

        th.displayData(nrQ, x);

    }

    public int getAverageWT() {
        if(clienti.size() == 0) return 0;
        return this.waitingTime/clienti.size();
    }


    public int getServiceTimeAv(){
        Iterator<Task> it = clienti.iterator();
        int sum = 0;
        Task aux;
        while (it.hasNext()){
            aux=it.next();
            sum+=aux.getServiceTime();
        }
        if(sum == 0) return 0;
        return sum/clienti.size();
    }

    @Override
    public void run() {

        while (true){
            if(clienti.remainingCapacity()!= this.maxNrTasksPerServer){
                try {
                    Task aux;
                    aux = clienti.peek();
                    sleep(aux.getServiceTime()*1000);  //sau fin time idk vede
                    this.waitingTime-=clienti.element().modifyWaitTime();
                    this.clienti.remove();

                    String x= "Q"+nrQ+" ";
                    Iterator<Task> tsk = clienti.iterator();
                    while(tsk.hasNext()){
                        Task t  = tsk.next();
                        x += t.makeTaskToStrig();
                        x+=" ";

                    }

                    th.displayData(nrQ, x);
                    System.out.println("/////////Task "+ aux.getID() + " left " );
                }
                catch (InterruptedException e) {

                }


            }



        }

    }

    public int getEmptyTime() {
        return emptyTime;
    }

    public int getAuxWait() {
        return auxWait;
    }
}
