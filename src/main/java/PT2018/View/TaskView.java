package PT2018.View;

import javafx.scene.control.PasswordField;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import static java.lang.Integer.*;

public class TaskView extends JFrame {


    private JTextField arrTimeMinEnter = new JTextField(5);
    private JTextField arrTimeMaxEnter = new JTextField(5);
    private JTextField serviceTimeMinEnter = new JTextField(5);
    private JTextField serviceTimeMaxEnter = new JTextField(5);
    private JTextField nrQEnter = new JTextField(5);
    private JTextField simulationIntervalEnter = new JTextField(5);
    private JTextField getNrClients = new JTextField(5);
    private JTextField averageWT = new JTextField(5);
    private JTextField peakTime = new JTextField(5);
    private JTextField averageServTime = new JTextField(5);
    private JTextField emptyTime = new JTextField(5);

    private ArrayList<JLabel> queues = new ArrayList<>();


    private JButton start = new JButton("Start");

    public JButton getButtonStart(){
        return start;
    }

    GridBagConstraints aranjare= new GridBagConstraints();

    public TaskView(){

        JPanel q = new JPanel(new GridBagLayout());

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(900, 1000);
        this.setTitle("Queue simulation");

        aranjare.insets = new Insets(10, 10, 10, 10);
        aranjare.gridy = 0;
        aranjare.gridx = 0;
        JLabel arrTime = new JLabel("Arriving time between customers:     min: ");
        q.add(arrTime, aranjare);

        aranjare.gridy = 0;
        aranjare.gridx =1;
        q.add(arrTimeMinEnter, aranjare);

        aranjare.gridy = 0;
        aranjare.gridx =2;
        JLabel arrTimeMax = new JLabel("max: ");
        q.add(arrTimeMax, aranjare);

        aranjare.gridy = 0;
        aranjare.gridx =3;
        q.add(arrTimeMaxEnter, aranjare);

        aranjare.gridy = 1;
        aranjare.gridx =0;
        JLabel serviceTime = new JLabel("Service time:                       min: " );
        q.add(serviceTime, aranjare);

        aranjare.gridy = 1;
        aranjare.gridx =1;
        q.add(serviceTimeMinEnter, aranjare);

        aranjare.gridy = 1;
        aranjare.gridx =2;
        JLabel serviceTimeMax = new JLabel("max: ");
        q.add(serviceTimeMax, aranjare);

        aranjare.gridy = 1;
        aranjare.gridx =3;
        q.add(serviceTimeMaxEnter, aranjare);

        aranjare.gridy = 1;
        aranjare.gridx =4;
        JLabel averageWTLabel = new JLabel("Average waiting time " );
        q.add( averageWTLabel, aranjare);

        aranjare.gridy = 3;
        aranjare.gridx =0;
        JLabel nrOfQs = new JLabel("Nr of queues to start with: ");
        q.add(nrOfQs, aranjare);

        aranjare.gridy = 3;
        aranjare.gridx =1;
        q.add(nrQEnter, aranjare);


        aranjare.gridy = 3;
        aranjare.gridx =4;
        q.add(averageWT, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx =0;
        JLabel simulationInterval = new JLabel("Simulation interval in miliseconds: ");
        q.add(simulationInterval, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx =1;
        q.add(simulationIntervalEnter, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx =4;
        JLabel peakTimeLabel = new JLabel("Peak time");
        q.add(peakTimeLabel, aranjare);

        aranjare.gridy = 5;
        aranjare.gridx =4;
        q.add(peakTime, aranjare);

        aranjare.gridy = 6;
        aranjare.gridx =4;
        JLabel servTimeL = new JLabel("Average Service time");
        q.add(servTimeL, aranjare);

        aranjare.gridy = 7;
        aranjare.gridx =4;
        q.add(averageServTime, aranjare);

        aranjare.gridy = 4;
        aranjare.gridx =7;
        q.add(emptyTime, aranjare);

        aranjare.gridy = 3;
        aranjare.gridx =7;
        JLabel x = new JLabel("Empty time");
        q.add(x, aranjare);


        aranjare.gridy = 5;
        aranjare.gridx =0;
        JLabel nrCl = new JLabel("Max nr of clients: ");
        q.add(nrCl, aranjare);

        aranjare.gridy = 5;
        aranjare.gridx =1;
        q.add(getNrClients, aranjare);

        aranjare.gridy = 6;
        aranjare.gridx =1;
        q.add(start, aranjare);

// trebuie sa implemetnz butonul start sa inceapa simularea

        aranjare.gridy = 8;
        aranjare.gridx =0;

        for(int i=0; i<10; i++){
            JLabel q1 = new JLabel("Q"+(i+1)+": ");
            q.add(q1, aranjare);
            aranjare.gridy ++;
            queues.add(q1);
        }








        this.add(q);


        this.setVisible(true);

    }

    public String getArrTimeMin (){
        return arrTimeMinEnter.getText();
    }
    public String getArrTimeMax (){
        return arrTimeMaxEnter.getText();
    }
    public String getServiceTimeMin (){
        return serviceTimeMinEnter.getText();
    }

    public String getServiceTimeMax (){
        return serviceTimeMaxEnter.getText();
    }


    public String getNrOfQ(){
        return nrQEnter.getText();
    }
    public String getSimInterval (){
        return simulationIntervalEnter.getText();
    }
    public String getNrClients (){
        return getNrClients.getText();
    }


    public void addStartListener(ActionListener listenForStartButton){
        start.addActionListener(listenForStartButton);
    }

    public void setAverageServTime(String av) {
        this.averageServTime.setText(av);
    }

    public void setAverageWaitTime(String averageWT) {
        this.averageWT.setText(averageWT);
    }

    public void setEmptyTime(String em) {
        this.emptyTime.setText(em);
    }

    public void setPeakTime(String peakTime) {
        this.peakTime.setText(peakTime);
    }




    public void displayData(int index, String evolution){


        for(int i=0; i<10; i++){
            if(i == index){
                queues.get(i).setText(evolution);
            }
        }


    }




}
