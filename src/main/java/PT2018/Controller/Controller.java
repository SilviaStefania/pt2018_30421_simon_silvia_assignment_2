package PT2018.Controller;


import PT2018.Model.SimulationManager;
import PT2018.View.TaskView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

    private TaskView theView;
    //private SimulationManager simulationManager;



    public Controller(TaskView theView ){

        this.theView=theView; // ore trebie schuimbata ordinea? ``/////////////////////////////////
        theView.addStartListener(new StartListener());

        //this.simulationManager = simulationManager;

    }




    class StartListener implements ActionListener {

        public void actionPerformed(ActionEvent arg0) {

            System.out.println("Simulation Starts");

            for(int i=0; i<10; i++){
                theView.displayData(i, "Q"+(i+1));
            }


            int timeLimit = Integer.parseInt(theView.getSimInterval()) ;
            int maxProcessingTime = Integer.parseInt(theView.getServiceTimeMax());
            int minProcessingTime = Integer.parseInt(theView.getServiceTimeMin()) ;
            int nrOfServers = Integer.parseInt(theView.getNrOfQ()); // cu cate incep deshise
            int numberOfClients = Integer.parseInt(theView.getNrClients()) ;
            int minARrTime  = Integer.parseInt(theView.getArrTimeMin());
            int maxArTime = Integer.parseInt(theView.getArrTimeMax());

            SimulationManager simulationManager = new SimulationManager(timeLimit,maxProcessingTime, minProcessingTime, nrOfServers, numberOfClients, maxArTime, minARrTime, theView);
            Thread t = new Thread(simulationManager);
            t.start();

            theView.setAverageServTime(simulationManager.getAvServTime());
            theView.setAverageWaitTime(simulationManager.getAvWaitTime());
            theView.setPeakTime(simulationManager.getPeak());
            theView.setEmptyTime("");
            theView.setAverageServTime("");
            theView.setPeakTime("");
            theView.setAverageWaitTime("");





            //  theView.afiseaza();
            // theView.displayData();





        }

    }




}
